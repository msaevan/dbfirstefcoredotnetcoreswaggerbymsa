﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace entities.Views
{
    public class Vw_GetStudents
    {
        [Key]
        public decimal ID { get; set; }
        public string StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
    }
}
