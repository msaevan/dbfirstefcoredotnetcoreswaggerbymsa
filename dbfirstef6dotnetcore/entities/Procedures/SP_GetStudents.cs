﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace entities.Procedures
{
    public class SP_GetStudents
    {
        [Key]
        public string StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
    }
}
