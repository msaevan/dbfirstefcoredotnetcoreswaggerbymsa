﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace entities.Tables
{
    public class TblStudent
    {
        [Key]
        public decimal ID { get; set; }
        public string StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
    }
}
