﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using business.Interfaces;
using datalayer.Context;
using entities.Procedures;
using entities.Tables;
using entities.Views;
using Microsoft.EntityFrameworkCore;

namespace business.Services
{
    public class StudentService : IStudentService
    {
        private readonly SampleDbContext _context;

        public StudentService(SampleDbContext context)
        {
            _context = context;
        }


        public void DeleteStudent(decimal studentId)
        {
            throw new NotImplementedException();
        }

        public List<SP_GetStudents> GetAllStudents()
        {
            var result = _context.SpGetStudents.FromSqlInterpolated($"SpGetStudents").ToList();

            return result;
        }

        public Vw_GetStudents GetStudentById(decimal studentId)
        {
            var result = _context.VwGetStudents.FirstOrDefault(x=>x.ID == studentId);

            return result;
        }

        public string GetStudentNameById(decimal studentId)
        {
            var result = _context.TblStudent.Where(x => x.ID == studentId).Select(x=> x.StudentName);
            return result.ToString();
        }

        public void SaveStudent(TblStudent objInputModel)
        {
            throw new NotImplementedException();
        }

        public void UpdateStudent(TblStudent objInputModel)
        {
            throw new NotImplementedException();
        }
    }
}
