﻿using System;
using System.Collections.Generic;
using System.Text;
using entities.Procedures;
using entities.Tables;
using entities.Views;

namespace business.Interfaces
{
    public interface IStudentService
    {
        void SaveStudent(TblStudent objInputModel);
        void UpdateStudent(TblStudent objInputModel);
        List<SP_GetStudents> GetAllStudents();
        void DeleteStudent(decimal studentId);
        Vw_GetStudents GetStudentById(decimal studentId);
        string GetStudentNameById(decimal studentId);
    }
}
