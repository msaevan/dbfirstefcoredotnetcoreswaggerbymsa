using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using business.Interfaces;
using business.Services;
using datalayer.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace dbfirstef6dotnetcore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //// 
            #region Swagger Service Specifications
            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc(
                    "LibraryOpenAPISpecification",
                    new Microsoft.OpenApi.Models.OpenApiInfo()
                    {
                        Title = "API Documentation",
                        Version = "1",
                        Description = "Test Descriptions...",
                        Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                        {
                            Name = "MSA EVAN",
                            Email = "msaevan.me@gmail.com",
                            Url = new Uri("https://www.linkedin.com/in/msaevan")
                        },
                        License = new Microsoft.OpenApi.Models.OpenApiLicense()
                        {
                            Name = "MIT Licence",
                            Url = new Uri("https://opensource.org/licenses/MIT")
                        },
                    }
                ); ;

                var xmlCommentsFile = $"{ System.Reflection.Assembly.GetExecutingAssembly().GetName().Name }.xml";
                var xmlCommentsFullPath = System.IO.Path.Combine(AppContext.BaseDirectory, xmlCommentsFile);

                setupAction.IncludeXmlComments(xmlCommentsFullPath);
            });

            #endregion

            ////
            #region Read Connection String & Define Service Scope 
            //
            //Fetching Connection string from APPSETTINGS.JSON  
            var connectionString = Configuration.GetConnectionString("SourceConnectionString");

            //Entity Framework
            services.AddDbContext<SampleDbContext>(opt =>
             opt.UseSqlServer(connectionString)
                .EnableSensitiveDataLogging()
             );

            //services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<StudentService>();

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            #region  Swagger Configuration
            app.UseSwagger();

            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint(
                    "/swagger/LibraryOpenAPISpecification/swagger.json",
                    "Library API"
                );
                setupAction.InjectStylesheet("custom-swagger-ui.css");
            });
            #endregion

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
