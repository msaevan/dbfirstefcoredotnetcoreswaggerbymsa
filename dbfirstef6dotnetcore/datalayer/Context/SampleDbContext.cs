﻿using System;
using System.Collections.Generic;
using System.Text;
using entities.Procedures;
using entities.Tables;
using entities.Views;
using Microsoft.EntityFrameworkCore;

namespace datalayer.Context
{
    public class SampleDbContext : DbContext
    {
        public SampleDbContext(DbContextOptions<SampleDbContext> options) : base(options)
        {
            ChangeTracker.LazyLoadingEnabled = false;
        }

        #region config for EF7 migrations (where we have little control over how resources are newed up)

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Entity Mapping with DB-Table
            builder.Entity<TblStudent>().ToTable("Table_Student");

            // Entity Mapping with store procedure.  
            //modelBuilder.Entity<SpBirthdayEntity>().MapToStoredProcedures();
            //modelBuilder.Types().Configure(t => t.MapToStoredProcedures());

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        #endregion

        #region Table Entities
        public virtual DbSet<TblStudent> TblStudent { get; set; }
        #endregion

        #region SP
        public virtual DbSet<SP_GetStudents> SpGetStudents { get; set; }
        #endregion

        #region VIEW
        public virtual DbSet<Vw_GetStudents> VwGetStudents { get; set; }
        #endregion
    }
}
